@extends('layouts.admintemplate') 
@section('member') 

@section('content')
<br>
<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">ชื่อ</th>
            <th scope="col">อีเมล์</th>
            <th scope="col">แต้ม</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Kritsana  Pradir</td>
            <td>kritsana.pr.60@ubu.ac.th</td>
            <td>147</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
        </tr>
    </tbody>
</table>

@endsection