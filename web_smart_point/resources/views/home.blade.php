@extends('layouts.usertemplate')

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> --}}


<br>
<br>
<div id="colorlib-page">
        <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
        <aside id="colorlib-aside" role="complementary" class="js-fullheight text-center">
            <h1 id="colorlib-logo"><a href="index.html">Smart Point<span>.</span></a></h1>
            <nav id="colorlib-main-menu" role="navigation">
                <ul>
                    <li class="colorlib-active"><a href="/">Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="#">Cafe Shop</a></li>
                </ul>
            </nav>

            <div class="colorlib-footer">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                {{-- <ul>
                    <li><a href="#"><i class="icon-facebook"></i></a></li>
                    <li><a href="#"><i class="icon-twitter"></i></a></li>
                    <li><a href="#"><i class="icon-instagram"></i></a></li>
                    <li><a href="#"><i class="icon-linkedin"></i></a></li>
                </ul> --}}
            </div>
        </aside> <!-- END COLORLIB-ASIDE -->
        <div id="colorlib-main">
            <div class="hero-wrap js-fullheight" style="background-image: url(images/bg_1.jpg);" data-stellar-background-ratio="0.5">
                <div class="overlay"></div>
                <div class="js-fullheight d-flex justify-content-center align-items-center">
                    <div class="col-md-8 text text-center">
                        <div class="img mb-4" style="background-image: url(images/author.jpg);"></div>
                        <button type="button" class="btn btn-info">แก้ไขรูปโปรไฟล์</button><br><br>
                        <center>
                        <div class="Point">
                            <h2 class="mb-4">พ้อย 1,518 แต้ม</h2>
                        </div>
                        </center><br>
                        <div class="desc">
                            <div class="profile">						
                                <div class="detail">
                                    <!-- <h2 class="mb-4">Piyawat Jangin</h2>
                                    <p class="mb-4">รหัสนักศึกษา 60114440198</p>
                                    <p class="mb-4">คณะ วิทยาศาสตร์</p> -->

                                    <table class="table">
                                            <thead class="thead-dark">
                                              <tr>
                                                <th scope="col">My Profile</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <tr>
                                                <th scope="row">ชื่อ</th>
                                                <td>นายปิยะวัฒน์ จังอินทร์</td>
                                              </tr>
                                              <tr>
                                                <th scope="row">รหัสประจำตัว</th>
                                                <td>60114440198</td>
                                              </tr>
                                              <tr>
                                                <th scope="row">คณะ</th>
                                                <td>วิทยาศาสตร์</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
 
@endsection
