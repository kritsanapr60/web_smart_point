@extends('layouts.welcometemplate')

@section('content')
<div class="row">
    <div class="col-md-8 mb-5">
        <h2>About Me : เกี่ยวกับฉัน</h2>
        <hr>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;เนื่องจากการซื้อน้ำจากร้านค้านั้น เช่นน้ำปั่น เเน่นอนที่จะต้องใส่เเก้วซึ่งเป็นเเก้วพลาสติก
                เเล้วใช้ได้เเค่ครั้งเดียว เราจึงอยากพัฒนา Sub - Project ที่ชื่อ Smart Point
                ขึ้นมาเพื่อช่วยลดปัญหาการใช้เเก้วมที่ร้านในมหาวิทยาลัย
                การเเก้ปัญหาคือให้นำเเก้วมาเองเพื่อแลกเเต้มและแต้มสามารถไปแลกเป็นส่วนลดในการซื้อน้ำครั้งต่อไ
                ปกับร้านที่ร่วมรายการอีกด้วย</p>
        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis optio neque consectetur consequatur magni in nisi, natus beatae quidem quam odit commodi ducimus totam eum, alias, adipisci nesciunt voluptate. Voluptatum.</p> -->
        
    </div>
    <div class="col-md-4 mb-5">
        <h2>Mobile Application</h2>
        <hr>
 <address>
  <!-- <img class="card-img-top" src="img/mobile.png" alt="" width="50" height="600"> -->
  <strong>Cros patform : Mobile Application.</strong>
  <br>ที่สามารถติดตั้งได้ทั้งระบบ IOS และ Androind
</address>
<center>
    <address>
        <img src="img/playst.png" alt="Play store" width="90" height="50">
        <img src="img/appst.png" alt="App store" width="120" height="70">
    </address>
</center>
    </div>
</div>
<!-- /.row -->
<div class="row">
        <div class="col-md-4 mb-5">
                <div class="card h-100">
                    <img class="card-img-top" src="http://placehold.it/300x200" alt="">
                    <div class="card-body">
                        <h5 class="card-title">นายกฤษณะ  ประสิทธิ์</h5>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="card h-100">
                    <img class="card-img-top" src="http://placehold.it/300x200" alt="">
                    <div class="card-body">
                        <h5 class="card-title">นายปิยะวัฒน์   จังอินทร์</h5>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-5">
                <div class="card h-100">
                    <img class="card-img-top" src="http://placehold.it/300x200" alt="">
                    <div class="card-body">
                        <h5 class="card-title">นายอธิป  ศิริ</h5>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
</div>
<!-- /.row -->
@endsection